/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package frameworks;

import java.util.ArrayList;
import java.util.Collections;

/**
 *
 * @author 2ndyrGroupA
 */
public class Exercise2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        ArrayList<String> animals = new ArrayList<String>();
        animals.add("Bear");
        animals.add("Echidna");
        animals.add("Donky's");
        animals.add("Chamelons");
        animals.add("Arapaima");
        animals.add("Bear");
        
        System.out.println(animals);
        Collections.shuffle(animals);
        System.out.println("Array after shuffle-------");
        System.out.println(animals);
    }

}
