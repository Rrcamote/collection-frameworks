/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package frameworks;

import java.util.Arrays;
import java.util.HashSet;

/**
 *
 * @author 2ndyrGroupA
 */
public class Exercise6 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        //number 6
        String[] arrayList = {"Baboy", "Kanding", "Manok", "Kabayo"};

        String[] arrayList1 = {"Langgam", "Kanding", "Baboy", "Kalabaw"};
   
        System.out.println("Animal-List1 : " + Arrays.toString(arrayList));
        System.out.println("Animal-List2 : " + Arrays.toString(arrayList1));

        HashSet<String> set = new HashSet<String>();

        for (int check1 = 0; check1 < arrayList.length; check1++) {
            for (int check2 = 0; check2 < arrayList1.length; check2++) {
                if (arrayList[check1].equals(arrayList1[check2])) {
                    set.add(arrayList[check1]);
                }
            }
        }

        System.out.println("Common Values : " + (set));
    }

}
