/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package frameworks;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 *
 * @author 2ndyrGroupA
 */
public class Exercise7 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here

        List<String> animalArray1 = Arrays.asList("Baboy", "Kanding", "Manok", "Kabayo");
        List<String> animalArray2 = Arrays.asList("Langgam", "Kanding", "Baboy", "Kalabaw");

        Set<String> commonUnion = new HashSet<String>(animalArray1);
        commonUnion.addAll(animalArray2);

        Set<String> intersection = new HashSet<String>(animalArray1);
        intersection.retainAll(animalArray2);

        commonUnion.removeAll(intersection);

        System.out.println("Array1" + animalArray1);
        System.out.println("Array1" + animalArray2);
        System.out.println("The unique of two array values is :" + "\n" + commonUnion);

    }

}
